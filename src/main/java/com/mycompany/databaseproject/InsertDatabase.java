/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author 11pro
 */
public class InsertDatabase {
    public static void main(String[] args) {
         Connection conn = null;
        String url = "jdbc:sqlite:dcoffee.db";
        //Connect Database
        try {
            conn = DriverManager.getConnection(url);
            System.err.println("Connection to SQLite has been establish.");
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
            return;
        }
        //Insert
        String sql = "INSERT INTO category(category_id, category_name) VALUES (?, ?)";
        try {
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, 3);
            pstmt.setString(2, "candy");
            pstmt.executeUpdate();
//            ResultSet key = pstmt.getGeneratedKeys();
//            key.next();
//            System.out.println("" + key.getInt(1));

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //Close Database
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.err.println(ex.getMessage());
            }
        }
    }

}
